http = require "http"
fs   = require "fs"
path = require "path"
mime = require "mime"

cache = {}

send404 = (response) ->
  response.writeHead 404, {"content-type": "text/plain"}
  response.write "error 404:resource not found."
  response.end()

sendFile = (response, filePath, contents) ->
  response.writeHead 200, {"content-type": mime.lookup(path.basename(filePath))}
  response.end contents

serverStatic = (response, absPath, cache) ->
  if cache[absPath]
    sendFile response, absPath, cache[absPath]
  else
    fs.exists absPath, (exists) ->
      if exists
        fs.readFile absPath, (err, data) ->
          console.log err
          if err
            send404 response
          else
            cache[absPath] = data
            sendFile response, absPath, data
      else
        send404 response

absolutePath = (request) ->
  if request.url == "/"
    "./public/index.html"
  else
    "./public" + request.url

server = http.createServer (request, response) ->
  absPath = absolutePath request
  serverStatic response, absPath, cache

server.listen 3000, ->
  console.log "Server running at http://localhost:3000/"
